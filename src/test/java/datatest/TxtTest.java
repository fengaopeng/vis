package datatest;

import com.vis.Application;
import com.vis.domain.*;
import com.vis.mapper.PieInfoMapper;
import com.vis.mapper.SalaryDegreeMapper;
import com.vis.service.*;
import com.vis.sqloperate.TxtTool;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest(classes = Application.class)
public class TxtTest {

    @Autowired
    private CityInfoService cityInfoService;

    @Autowired
    private MapInfoService mapInfoService;

    @Autowired
    private PositionInfoService positionInfoService;

    @Autowired
    private PieInfoService pieInfoService;

    @Autowired
    private DegreeSalaryService degreeSalaryService;

    @Autowired
    private SalaryDegreeService salaryDegreeService;

    @Autowired
    private BoxwireService boxwireService;

    @Autowired
    private FunnelService funnelService;
//    @Test
//    void saveCity(){
//        TxtTool txtTool = new TxtTool();
//        List<CityInfo> cityInfoList = txtTool.readTxt("C:\\Users\\86176\\Desktop\\可视化2\\可视化api\\vis\\src\\main\\java\\com\\vis\\sqloperate\\docs\\data.txt");
//        for (CityInfo cityInfo : cityInfoList) {
//            cityInfoService.save(cityInfo);
//        }
//    }
//
//    @Test
//    void saveHeatMap(){
//        TxtTool txtTool = new TxtTool();
//        List<MapInfo> mapInfoList = txtTool.readMapTxt("C:\\Users\\86176\\Desktop\\可视化2\\可视化api\\vis\\src\\main\\java\\com\\vis\\sqloperate\\docs\\招聘发布时间统计.txt");
//        for (MapInfo mapInfo : mapInfoList) {
//            mapInfoService.save(mapInfo);
//        }
//    }
//

//    @Test
//    void savePosition(){
//        TxtTool txtTool = new TxtTool();
//        List<PositionInfo> positionInfoList = txtTool.readPositionTxt("C:\\Users\\86176\\Desktop\\可视化2\\可视化api\\vis\\src\\main\\java\\com\\vis\\sqloperate\\docs\\词频.txt");
//
//        for (PositionInfo positionInfo : positionInfoList) {
//            positionInfoService.save(positionInfo);
//        }
//    }

//    @Test
//    void savePieInfo(){
//          TxtTool txtTool = new TxtTool();
//          List<PieInformation> pieInformationList = txtTool.readPieTxt("C:\\Users\\86176\\Desktop\\可视化2\\可视化api\\vis\\src\\main\\java\\com\\vis\\sqloperate\\docs\\统计信息1.txt");
//        for (PieInformation pieInformation : pieInformationList) {
//           pieInfoService.save(pieInformation);
//        }
//    }

//    @Test
//    void saveDegreeInfo(){
//          TxtTool txtTool = new TxtTool();
//          List<DegreeSalary> degreeSalaryList = txtTool.readDegreeTxt("C:\\Users\\86176\\Desktop\\可视化2\\可视化api\\vis\\src\\main\\java\\com\\vis\\sqloperate\\docs\\学历看薪资1.txt");
//        for (DegreeSalary degreeSalary : degreeSalaryList) {
//            degreeSalaryService.save(degreeSalary);
//        }
//    }

//    @Test
//    void saveSalaryInfo(){
//        TxtTool txtTool = new TxtTool();
//        List<SalaryDegree> salaryDegreeList = txtTool.readSalaryTxt("C:\\Users\\86176\\Desktop\\可视化2\\可视化api\\vis\\src\\main\\java\\com\\vis\\sqloperate\\docs\\薪资看学历1.txt");
//        for (SalaryDegree salaryDegree : salaryDegreeList) {
//            salaryDegreeService.save(salaryDegree);
//        }
//    }

//        @Test
//        void saveBoxInfo(){
//            TxtTool txtTool = new TxtTool();
//            List<Boxwire> boxwireList = txtTool.readBoxTxt("C:\\Users\\86176\\Desktop\\可视化2\\可视化api\\vis\\src\\main\\java\\com\\vis\\sqloperate\\docs\\工作经验1.txt");
//            for (Boxwire boxwire : boxwireList) {
//                boxwireService.save(boxwire);
//            }
//    }

//    @Test
//    void saveBoxInfo(){
//        TxtTool txtTool = new TxtTool();
//        List<Funnel> funnelList = txtTool.readFunnelTxt("C:\\Users\\86176\\Desktop\\可视化2\\可视化api\\vis\\src\\main\\java\\com\\vis\\sqloperate\\docs\\融资1.txt");
//        for (Funnel funnel : funnelList) {
//            funnelService.save(funnel);
//        }
//    }


}
