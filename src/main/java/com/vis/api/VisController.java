package com.vis.api;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.vis.common.PieVo;
import com.vis.common.R;
import com.vis.common.ResultVo;
import com.vis.domain.*;
import com.vis.mapper.*;
import com.vis.service.BoxwireService;
import com.vis.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.sound.sampled.Line;
import java.util.*;
@Transactional
@RestController
@CrossOrigin(origins = "*")
public class VisController {
    @Autowired
    private GenerallnformationMapper generallnformationMapper;

    @Autowired
    private CityInfoMapper cityInfoMapper;

    @Autowired
    private MapInfoMapper mapInfoMapper;

    @Autowired
    private PositionInfoMapper positionInfoMapper;

    @Autowired
    private PieInfoMapper pieInfoMapper;

    @Autowired
    private DegreeSalaryMapper degreeSalaryMapper;

    @Autowired
    private SalaryDegreeMapper salaryDegreeMapper;

    @Autowired
    private BoxwireMapper boxwireMapper;

    @Autowired
    private FunnelMapper funnelMapper;

    //@CrossOrigin(origins = "*")
    //已完成
    @GetMapping("/getGeneralInformation")
    public R<Object> getGeneralInformation(){
        QueryWrapper<GeneralInformation> generalInformationQueryWrapper = new QueryWrapper<>();
        List<GeneralInformation> generalInformationList = generallnformationMapper.selectList(generalInformationQueryWrapper);
        List<GeneralInformationVo> generalInformationVoList = new ArrayList<>();
        for (GeneralInformation generalInformation : generalInformationList) {
            GeneralInformationVo generalInformationVo = new GeneralInformationVo();
            generalInformationVo.setMsgName(generalInformation.getName());
            generalInformationVo.setMsgData(generalInformation.getCount());
            generalInformationVoList.add(generalInformationVo);
        }
        return R.success(generalInformationVoList);

    }

    //@CrossOrigin(origins = "*")
    //已完成
    @GetMapping("/getMapData")
    public ResultVo<Object> getMapData(){
        QueryWrapper<CityInfo> cityInfoQueryWrapper = new QueryWrapper<>();
        List<CityInfo> cityInfoList = cityInfoMapper.selectList(cityInfoQueryWrapper);
        List<CityInfoVo> cityInfoVoList = new ArrayList<>();
        Map<String ,String> geoMap = new HashMap<>();
        StringBuilder builder = new StringBuilder();
        for (CityInfo cityInfo : cityInfoList) {
            CityInfoVo cityInfoVo = new CityInfoVo();
            cityInfoVo.setName(cityInfo.getCityName());
            cityInfoVo.setValue(cityInfo.getCount());
            cityInfoVoList.add(cityInfoVo);
            geoMap.put(cityInfo.getCityName(),"["+cityInfo.getLng()+","+cityInfo.getLat()+"]");
        }
        return ResultVo.success(cityInfoVoList,geoMap);
    }

    //@CrossOrigin(origins = "*")
    //已完成
    @GetMapping("/getHeatMap")
    public R<Object> getHeatMap(){
        QueryWrapper<MapInfo> mapInfoQueryWrapper = new QueryWrapper<>();
        List<MapInfo> mapInfoList = mapInfoMapper.selectList(mapInfoQueryWrapper);
        return R.success(mapInfoList);
    }

    //@CrossOrigin
    @GetMapping("/getPositionType")
    public R<Object> getPositionType(){
        QueryWrapper<PositionInfo> positionInfoQueryWrapper = new QueryWrapper<>();
        List<PositionInfo> positionInfoList = positionInfoMapper.selectList(positionInfoQueryWrapper);
        List<PostInfoVo> postInfoVoList = new ArrayList<>();
        for (PositionInfo positionInfo : positionInfoList) {
            PostInfoVo postInfoVo = new PostInfoVo();
            postInfoVo.setName(positionInfo.getName());
            postInfoVo.setValue(positionInfo.getNum());
            postInfoVoList.add(postInfoVo);
        }
        return R.success(postInfoVoList);
    }

    @GetMapping("/getPieSelection")
    public R<Object> getPieSelection(){
        //要序列化的列表
        List<LableObj> lableObjs = new ArrayList<>();

        //拿到数据库的所有城市，set存储
        QueryWrapper<PieInformation> pieInformationQueryWrapper = new QueryWrapper<>();
        //Set<String> citySet = new HashSet<>();
        List<PieInformation> pieInformationList = pieInfoMapper.selectList(pieInformationQueryWrapper);
        Map<String,List<String>> map = new HashMap<>();
        for (PieInformation pieInformation : pieInformationList) {
            if (map.containsKey(pieInformation.getName())){
                List<String> tempList = map.get(pieInformation.getName());
                tempList.add(pieInformation.getType());
                map.put(pieInformation.getName(),tempList);
            }else {
                List<String> tempList = new ArrayList<>();
                tempList.add(pieInformation.getType());
                map.put(pieInformation.getName(),tempList);
            }
        }
        Set<String> mapKey = map.keySet();
        for (String s : mapKey){
            LableObj lableObj = new LableObj();
            lableObj.setLabel(s);
            lableObj.setValue(s);
            List<LableData> lableDataList = new ArrayList<>();
            List<String> tempList1 = map.get(s);
            for (String s1 : tempList1) {
                LableData lableData = new LableData();
                lableData.setLabel(s1);
                lableData.setValue(s1);
                lableDataList.add(lableData);
            }
            lableObj.setChildren(lableDataList);
            lableObjs.add(lableObj);
        }

        //从set遍历出城市的所有子数据
//       for (String s : citySet){
//           LambdaQueryWrapper<PieInformation> lambdaQueryWrapper = new LambdaQueryWrapper<>();
//           lambdaQueryWrapper.eq(PieInformation::getName,s);
//            List<PieInformation> pieInformationList1 = pieInfoMapper.selectList(pieInformationQueryWrapper);
//            List<LableData> lableDataList = new ArrayList<>();
//            for (PieInformation pieInformation : pieInformationList1) {
//                LableData lableData = new LableData();
//                lableData.setLable(pieInformation.getType());
//                lableData.setValue(pieInformation.getType());
//                lableDataList.add(lableData);
//            }
//            LableObj obj = new LableObj();
//            obj.setChildren(lableDataList);
//            obj.setValue(s);
//            obj.setLable(s);
//            lableObjs.add(obj);
//        }


        return R.success(lableObjs);
    }
    @GetMapping("/getBarLineData")
    public R<Object> getBarLineData(){
        QueryWrapper<PieInformation> pieInformationQueryWrapper = new QueryWrapper<>();
        List<PieInformation> pieInformationList = pieInfoMapper.selectList(pieInformationQueryWrapper);
        Map<String,Double> map = new HashMap<>();
        Map<String,Integer> mapNum = new HashMap<>();

        for (PieInformation pieInformation : pieInformationList) {
            if (map.containsKey(pieInformation.getName())){
                Double temp = map.get(pieInformation.getName());
                Double temp1 = temp+(pieInformation.getLevel1()*2.5+pieInformation.getLevel2()*7.5+pieInformation.getLevel3()*12.5+pieInformation.getLevel4()*17.5);
                map.put(pieInformation.getName(),temp1);

                Integer numTemp = mapNum.get(pieInformation.getName());
                Integer numTemp1 = numTemp+pieInformation.getLevel1()+pieInformation.getLevel2()+pieInformation.getLevel3()+pieInformation.getLevel4();
                mapNum.put(pieInformation.getName(),numTemp1);
            }else {
                Double temp = pieInformation.getLevel1()*2.5+pieInformation.getLevel2()*7.5+pieInformation.getLevel3()*12.5+pieInformation.getLevel4()*17.5;
                map.put(pieInformation.getName(),temp);

                Integer numTemp = pieInformation.getLevel1()+pieInformation.getLevel2()+pieInformation.getLevel3()+pieInformation.getLevel4();
                mapNum.put(pieInformation.getName(),numTemp);
            }
        }
        List<BarVo> barVoList = new ArrayList<>();
        List<LineVo> lineVoList = new ArrayList<>();
        Set<String> mapKey = map.keySet();
        for (String s : mapKey){
            BarVo barVo = new BarVo();
            barVo.setCityName(s);
            Double temp = map.get(s)/mapNum.get(s);
            Double temp1 = Double.valueOf(String.format("%.1f",temp));
            barVo.setCityValue(temp1);
            barVoList.add(barVo);
            LineVo lineVo = new LineVo();
            lineVo.setCityName(s);
            lineVo.setCityValue(mapNum.get(s));
            lineVoList.add(lineVo);
        }
        barVoList.sort((x,y) ->Double.compare(x.getCityValue(),y.getCityValue()));
        lineVoList.sort((x,y) ->Integer.compare(x.getCityValue(),y.getCityValue()));
        List<BarVo> barVoList1 = new ArrayList<>();
        List<LineVo> lineVoList1 = new ArrayList<>();
        int k = 0;
        for (int i = barVoList.size()-1; i >= 0; i--) {
            BarVo barVo1 = barVoList.get(i);
            barVoList1.add(barVo1);
            LineVo lineVo = lineVoList.get(i);
            lineVoList1.add(lineVo);
            k++;
            if (k==20){
                break;
            }
        }
        return R.success(barVoList1,lineVoList1);
    }

    @GetMapping("/getPieData")
    public PieVo<Object> getPieData(@RequestParam String city){
        String[] arr = city.split("/");
        String confirm = "大专学历不限本科硕士";
        String confirm1 = "5k以下5k-10k10k-15k15k以上";
        if (arr.length==2){
            LambdaQueryWrapper<PieInformation> lambdaQueryWrapper = new LambdaQueryWrapper<>();
            lambdaQueryWrapper.eq(PieInformation::getName,arr[0]);
            lambdaQueryWrapper.eq(PieInformation::getType,arr[1]);
            PieInformation pieInformation = pieInfoMapper.selectOne(lambdaQueryWrapper);
            List<PieInfoVo> pieInfoVos = new ArrayList<>();
            List<PieInfoVo> pieInfoVos1 = new ArrayList<>();

            PieInfoVo pieInfoVo = new PieInfoVo();
            pieInfoVo.setName("大专");
            pieInfoVo.setValue(pieInformation.getVocation());
            pieInfoVos.add(pieInfoVo);
            PieInfoVo pieInfoVo1 = new PieInfoVo();
            pieInfoVo1.setName("学历不限");
            pieInfoVo1.setValue(pieInformation.getUnlimited());
            pieInfoVos.add(pieInfoVo1);
            PieInfoVo pieInfoVo2 = new PieInfoVo();
            pieInfoVo2.setName("本科");
            pieInfoVo2.setValue(pieInformation.getUndergraduate());
            pieInfoVos.add(pieInfoVo2);
            PieInfoVo pieInfoVo3 = new PieInfoVo();
            pieInfoVo3.setName("硕士");
            pieInfoVo3.setValue(pieInformation.getMaster());
            pieInfoVos.add(pieInfoVo3);

            PieInfoVo pieInfoVo4 = new PieInfoVo();
            pieInfoVo4.setName("5k以下");
            pieInfoVo4.setValue(pieInformation.getLevel1());
            pieInfoVos1.add(pieInfoVo4);
            PieInfoVo pieInfoVo5 = new PieInfoVo();
            pieInfoVo5.setName("5k-10k");
            pieInfoVo5.setValue(pieInformation.getLevel2());
            pieInfoVos1.add(pieInfoVo5);
            PieInfoVo pieInfoVo6 = new PieInfoVo();
            pieInfoVo6.setName("10k-15k");
            pieInfoVo6.setValue(pieInformation.getLevel3());
            pieInfoVos1.add(pieInfoVo6);
            PieInfoVo pieInfoVo7 = new PieInfoVo();
            pieInfoVo7.setName("15k以上");
            pieInfoVo7.setValue(pieInformation.getLevel4());
            pieInfoVos1.add(pieInfoVo7);

            return PieVo.success("success",pieInfoVos,pieInfoVos1);
            }
            else if (arr.length==3){
                if (confirm.contains(arr[2])){
                    LambdaQueryWrapper<DegreeSalary> degreeSalaryLambdaQueryWrapper = new LambdaQueryWrapper<>();
                    degreeSalaryLambdaQueryWrapper.eq(DegreeSalary::getName,arr[0]);
                    degreeSalaryLambdaQueryWrapper.eq(DegreeSalary::getType,arr[1]);
                    degreeSalaryLambdaQueryWrapper.eq(DegreeSalary::getDegree,arr[2]);
                    DegreeSalary degreeSalary = degreeSalaryMapper.selectOne(degreeSalaryLambdaQueryWrapper);
                    List<PieInfoVo> pieInfoVos1 = new ArrayList<>();
                    List<PieInfoVo> pieInfoVos = new ArrayList<>();
                    PieInfoVo pieInfoVo4 = new PieInfoVo();
                    pieInfoVo4.setName("5k以下");
                    pieInfoVo4.setValue(degreeSalary.getLevel1());
                    pieInfoVos1.add(pieInfoVo4);
                    PieInfoVo pieInfoVo5 = new PieInfoVo();
                    pieInfoVo5.setName("5k-10k");
                    pieInfoVo5.setValue(degreeSalary.getLevel2());
                    pieInfoVos1.add(pieInfoVo5);
                    PieInfoVo pieInfoVo6 = new PieInfoVo();
                    pieInfoVo6.setName("10k-15k");
                    pieInfoVo6.setValue(degreeSalary.getLevel3());
                    pieInfoVos1.add(pieInfoVo6);
                    PieInfoVo pieInfoVo7 = new PieInfoVo();
                    pieInfoVo7.setName("15k以上");
                    pieInfoVo7.setValue(degreeSalary.getLevel4());
                    pieInfoVos1.add(pieInfoVo7);
                    return PieVo.success("success",pieInfoVos,pieInfoVos1);
                }
                else if (confirm1.contains(arr[2])){

                    LambdaQueryWrapper<SalaryDegree> salaryDegreeLambdaQueryWrapper = new LambdaQueryWrapper<>();
                    salaryDegreeLambdaQueryWrapper.eq(SalaryDegree::getName,arr[0]);
                    salaryDegreeLambdaQueryWrapper.eq(SalaryDegree::getType,arr[1]);
                    salaryDegreeLambdaQueryWrapper.eq(SalaryDegree::getSalary,arr[2]);
                    SalaryDegree salaryDegree = salaryDegreeMapper.selectOne(salaryDegreeLambdaQueryWrapper);
                    List<PieInfoVo> pieInfoVos = new ArrayList<>();
                    List<PieInfoVo> pieInfoVos1 = new ArrayList<>();

                    PieInfoVo pieInfoVo = new PieInfoVo();
                    pieInfoVo.setName("大专");
                    pieInfoVo.setValue(salaryDegree.getVocation());
                    pieInfoVos.add(pieInfoVo);
                    PieInfoVo pieInfoVo1 = new PieInfoVo();
                    pieInfoVo1.setName("学历不限");
                    pieInfoVo1.setValue(salaryDegree.getUnlimited());
                    pieInfoVos.add(pieInfoVo1);
                    PieInfoVo pieInfoVo2 = new PieInfoVo();
                    pieInfoVo2.setName("本科");
                    pieInfoVo2.setValue(salaryDegree.getUndergraduate());
                    pieInfoVos.add(pieInfoVo2);
                    PieInfoVo pieInfoVo3 = new PieInfoVo();
                    pieInfoVo3.setName("硕士");
                    pieInfoVo3.setValue(salaryDegree.getMaster());
                    pieInfoVos.add(pieInfoVo3);
                    return PieVo.success("success",pieInfoVos,pieInfoVos1);
                }

            }




        return PieVo.success("ok",null,null);
    }

    @GetMapping("/getBoxwire")
    public R<Object> getBoxwire(){
        QueryWrapper<Boxwire> queryWrapper = new QueryWrapper<>();
        List<Boxwire> boxwireList = boxwireMapper.selectList(queryWrapper);
        return R.success(boxwireList);
    }

    @GetMapping("/getFunnelData")
    public R<Object> getFunnelData(@RequestParam String city){
        List<FunnelVo> funnelVoList = new ArrayList<>();
        LambdaQueryWrapper<Funnel> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Funnel::getName,city);
        Funnel funnel = funnelMapper.selectOne(queryWrapper);

        FunnelVo funnelVo = new FunnelVo();
        funnelVo.setStage("上市公司");
        funnelVo.setNumber(funnel.getValue1());
        if (funnelVo.getNumber()!=0){
            funnelVoList.add(funnelVo);
        }


        FunnelVo funnelVo1 = new FunnelVo();
        funnelVo1.setStage("初创型(不需要融资)");
        funnelVo1.setNumber(funnel.getValue2());
        if (funnelVo1.getNumber()!=0){
            funnelVoList.add(funnelVo1);
        }

        FunnelVo funnelVo2 = new FunnelVo();
        funnelVo2.setStage("初创型(天使轮)");
        funnelVo2.setNumber(funnel.getValue3());
        if (funnelVo2.getNumber()!=0){
            funnelVoList.add(funnelVo2);
        }

        FunnelVo funnelVo3 = new FunnelVo();
        funnelVo3.setStage("初创型(未融资)");
        funnelVo3.setNumber(funnel.getValue4());
        if (funnelVo3.getNumber()!=0){
            funnelVoList.add(funnelVo3);
        }

        FunnelVo funnelVo4 = new FunnelVo();
        funnelVo4.setStage("成熟型(C轮)");
        funnelVo4.setNumber(funnel.getValue5());
        if (funnelVo4.getNumber()!=0){
            funnelVoList.add(funnelVo4);
        }

        FunnelVo funnelVo5 = new FunnelVo();
        funnelVo5.setStage("成熟型(D轮及以上)");
        funnelVo5.setNumber(funnel.getValue6());
        if (funnelVo5.getNumber()!=0){
            funnelVoList.add(funnelVo5);
        }

        FunnelVo funnelVo6 = new FunnelVo();
        funnelVo6.setStage("成熟型(不需要融资)");
        funnelVo6.setNumber(funnel.getValue7());
        if (funnelVo6.getNumber()!=0){
            funnelVoList.add(funnelVo6);
        }

        FunnelVo funnelVo7 = new FunnelVo();
        funnelVo7.setStage("成长型(A轮)");
        funnelVo7.setNumber(funnel.getValue8());
        if (funnelVo7.getNumber()!=0){
            funnelVoList.add(funnelVo7);
        }

        FunnelVo funnelVo8 = new FunnelVo();
        funnelVo8.setStage("成长型(B轮)");
        funnelVo8.setNumber(funnel.getValue9());
        if (funnelVo8.getNumber()!=0){
            funnelVoList.add(funnelVo8);
        }

        FunnelVo funnelVo9 = new FunnelVo();
        funnelVo9.setStage("成长型(不需要融资)");
        funnelVo9.setNumber(funnel.getValue10());
        if (funnelVo9.getNumber()!=0){
            funnelVoList.add(funnelVo9);
        }
        return R.success(funnelVoList);
    }

    
}
