package com.vis.vo;

import lombok.Data;

@Data
public class PostInfoVo {
    private String name;

    private Integer value;
}
