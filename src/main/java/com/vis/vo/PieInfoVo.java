package com.vis.vo;

import lombok.Data;

@Data
public class PieInfoVo {
    private String name;

    private Integer value;
}
