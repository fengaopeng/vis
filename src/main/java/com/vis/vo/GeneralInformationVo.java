package com.vis.vo;

import lombok.Data;

@Data
public class GeneralInformationVo {
    private String msgName;

    private Integer msgData;
}
