package com.vis.vo;

import lombok.Data;

@Data
public class CityInfoVo {
    private String name;

    private String value;
}
