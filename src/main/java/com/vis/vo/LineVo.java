package com.vis.vo;

import lombok.Data;

@Data
public class LineVo {
    private String cityName;

    private Integer cityValue;
}
