package com.vis.vo;

import lombok.Data;

@Data
public class FunnelVo {
    private String stage;

    private Integer number;
}
