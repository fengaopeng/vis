package com.vis.sqloperate;

import com.vis.domain.PostInfo;

import java.io.IOException;
import java.util.*;

public class main {
    public static void main(String[] args) throws IOException {

        Map<String,List<String>> map = new HashMap<>();
        List<String> tempList = new ArrayList<>();
        tempList.add("111");
        tempList.add("222");
        map.put("test",tempList);
        if (map.containsKey("test")){
            List<String> tempList1 = map.get("test");
            System.out.println("第一次遍历");
            for (String s : tempList1) {
                System.out.println(s);
            }
            tempList1.add("333");
            map.put("test",tempList1);
        }
        Set<String> mapKey = map.keySet();
        for (String s : mapKey){
            List<String> tempList2 = map.get(s);
            System.out.println("第二次遍历");
            for (String s1 : tempList2) {
                System.out.println(s1);
            }
        }
    }
}
