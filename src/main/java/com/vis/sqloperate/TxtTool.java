package com.vis.sqloperate;

import com.vis.domain.*;
import com.vis.service.DegreeSalaryService;
import org.apache.poi.ss.formula.functions.T;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TxtTool {
    public  List<CityInfo> readTxt(String txtPath) {
        File file = new File(txtPath);
        List<CityInfo> cityInfoList = new ArrayList<>();
        if(file.isFile() && file.exists()){
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                StringBuffer sb = new StringBuffer();
                String text = null;
                while((text = bufferedReader.readLine()) != null){
                    System.out.println(text);
                    String[] arr = text.split("\\s+");
                    CityInfo cityInfo = new CityInfo();
                    cityInfo.setCityName(arr[0]);
                    cityInfo.setCount(arr[1]);
                    cityInfo.setLng(Double.parseDouble(arr[2]));
                    cityInfo.setLat(Double.parseDouble(arr[3]));
//                    System.out.println(arr[0]);
//                    System.out.println(arr[1]);
//                    System.out.println(arr[2]);
//                    System.out.println(arr[3]);
                    cityInfoList.add(cityInfo);

                }
                return cityInfoList;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    public  List<MapInfo> readMapTxt(String txtPath) {
        File file = new File(txtPath);
        List<MapInfo> mapInfoList = new ArrayList<>();
        if(file.isFile() && file.exists()){
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                StringBuffer sb = new StringBuffer();
                String text = null;
                while((text = bufferedReader.readLine()) != null){
                    System.out.println(text);
                    String[] arr = text.split(",");
                    if (arr[0].equals("CreateTime")){
                        continue;
                    }
                    MapInfo mapInfo = new MapInfo();
                    mapInfo.setTime(arr[0]);
                    mapInfo.setNum(Integer.parseInt(arr[1]));
                    mapInfoList.add(mapInfo);

                }
                return mapInfoList;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public  List<PositionInfo> readPositionTxt(String txtPath) {
        File file = new File(txtPath);
        List<PositionInfo> positionInfoList = new ArrayList<>();
        if(file.isFile() && file.exists()){
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                StringBuffer sb = new StringBuffer();
                String text = null;
                while((text = bufferedReader.readLine()) != null){
                    System.out.println(text);

                    String[] arr = text.split(",");
                    PositionInfo positionInfo = new PositionInfo();
                    positionInfo.setName(arr[0]);
                    positionInfo.setNum(Integer.parseInt(arr[1]));
                    positionInfoList.add(positionInfo);

                }
                return positionInfoList;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    public  List<PieInformation> readPieTxt(String txtPath) {
        File file = new File(txtPath);
        List<PieInformation> pieInformationList = new ArrayList<>();
        if(file.isFile() && file.exists()){
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                StringBuffer sb = new StringBuffer();
                String text = null;
                while((text = bufferedReader.readLine()) != null){
                    System.out.println(text);

                    String[] arr = text.split(",");
                    if (arr[0].equals("City")){
                        continue;
                    }
                    PieInformation pieInformation = new PieInformation();
                    pieInformation.setName(arr[0]);
                    pieInformation.setType(arr[1]);
                    pieInformation.setVocation(Integer.parseInt(arr[2]));
                    pieInformation.setUnlimited(Integer.parseInt(arr[3]));
                    pieInformation.setUndergraduate(Integer.parseInt(arr[4]));
                    pieInformation.setMaster(Integer.parseInt(arr[5]));
                    pieInformation.setLevel3(Integer.parseInt(arr[6]));
                    pieInformation.setLevel4(Integer.parseInt(arr[7]));
                    pieInformation.setLevel2(Integer.parseInt(arr[8]));
                    pieInformation.setLevel1(Integer.parseInt(arr[9]));
                    pieInformationList.add(pieInformation);

                }
                return pieInformationList;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    public  List<DegreeSalary> readDegreeTxt(String txtPath) {
        File file = new File(txtPath);
        List<DegreeSalary> degreeSalaryList = new ArrayList<>();
        if(file.isFile() && file.exists()){
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                StringBuffer sb = new StringBuffer();
                String text = null;
                while((text = bufferedReader.readLine()) != null){
                    System.out.println(text);

                    String[] arr = text.split(",");
                    if (arr[0].equals("City")){
                        continue;
                    }
                    DegreeSalary degreeSalary = new DegreeSalary();
                    degreeSalary.setName(arr[0]);
                    degreeSalary.setType(arr[1]);
                    degreeSalary.setDegree(arr[2]);
                    degreeSalary.setLevel3(Integer.parseInt(arr[3]));
                    degreeSalary.setLevel4(Integer.parseInt(arr[4]));
                    degreeSalary.setLevel2(Integer.parseInt(arr[5]));
                    degreeSalary.setLevel1(Integer.parseInt(arr[6]));
                    degreeSalaryList.add(degreeSalary);
                }
                return degreeSalaryList;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public  List<SalaryDegree> readSalaryTxt(String txtPath) {
        File file = new File(txtPath);
        List<SalaryDegree> salaryDegreeList = new ArrayList<>();
        if(file.isFile() && file.exists()){
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                StringBuffer sb = new StringBuffer();
                String text = null;
                while((text = bufferedReader.readLine()) != null){
                    System.out.println(text);

                    String[] arr = text.split(",");
                    if (arr[0].equals("City")){
                        continue;
                    }
                    SalaryDegree salaryDegree = new SalaryDegree();
                    salaryDegree.setName(arr[0]);
                    salaryDegree.setType(arr[1]);
                    salaryDegree.setSalary(arr[2]);
                    salaryDegree.setVocation(Integer.parseInt(arr[3]));
                    salaryDegree.setUnlimited(Integer.parseInt(arr[4]));
                    salaryDegree.setUndergraduate(Integer.parseInt(arr[5]));
                    salaryDegree.setMaster(Integer.parseInt(arr[6]));
                    salaryDegreeList.add(salaryDegree);
                }
                return salaryDegreeList;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public  List<Boxwire> readBoxTxt(String txtPath) {
        File file = new File(txtPath);
        List<Boxwire> boxwireList = new ArrayList<>();
        if(file.isFile() && file.exists()){
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                StringBuffer sb = new StringBuffer();
                String text = null;
                while((text = bufferedReader.readLine()) != null){
                    System.out.println(text);

                    String[] arr = text.split(",");
                    if (arr[0].equals("WorkYear")){
                        continue;
                    }
                    Boxwire boxwire = new Boxwire();
                    boxwire.setX(arr[0]);
                    boxwire.setLow(Integer.parseInt(arr[1]));
                    boxwire.setQ1(Integer.parseInt(arr[2]));
                    boxwire.setMedian(Integer.parseInt(arr[3]));
                    boxwire.setQ3(Integer.parseInt(arr[4]));
                    boxwire.setHigh(Integer.parseInt(arr[5]));
                    boxwireList.add(boxwire);
                }
                return boxwireList;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public  List<Funnel> readFunnelTxt(String txtPath) {
        File file = new File(txtPath);
        List<Funnel> funnelList = new ArrayList<>();
        if(file.isFile() && file.exists()){
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                StringBuffer sb = new StringBuffer();
                String text = null;
                while((text = bufferedReader.readLine()) != null){
                    System.out.println(text);

                    String[] arr = text.split(",");
                    if (arr[0].equals("City")){
                        continue;
                    }
                    Funnel funnel = new Funnel();
                    funnel.setName(arr[0]);
                    funnel.setValue1(Integer.parseInt(arr[1]));
                    funnel.setValue2(Integer.parseInt(arr[2]));
                    funnel.setValue3(Integer.parseInt(arr[3]));
                    funnel.setValue4(Integer.parseInt(arr[4]));
                    funnel.setValue5(Integer.parseInt(arr[5]));
                    funnel.setValue6(Integer.parseInt(arr[6]));
                    funnel.setValue7(Integer.parseInt(arr[7]));
                    funnel.setValue8(Integer.parseInt(arr[8]));
                    funnel.setValue9(Integer.parseInt(arr[9]));
                    funnel.setValue10(Integer.parseInt(arr[10]));
                    funnelList.add(funnel);
                }
                return funnelList;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
