package com.vis.sqloperate;

import com.csvreader.CsvReader;
import com.vis.domain.PostInfo;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class CsvTool {

    public List<PostInfo> read() throws IOException {

        // 第一参数：读取文件的路径 第二个参数：分隔符（不懂仔细查看引用百度百科的那段话） 第三个参数：字符集
        CsvReader csvReader = new CsvReader("C:\\Users\\86176\\Desktop\\可视化2\\可视化api\\vis\\src\\main\\docs\\附件1：招聘信息.csv", ',', Charset.forName("GBK"));

        // 如果你的文件没有表头，这行不用执行
        // 这行不要是为了从表头的下一行读，也就是过滤表头
        csvReader.readHeaders();
        // 读取每行的内容
        List<PostInfo> postInfoList = new ArrayList<>();
        while (csvReader.readRecord()) {
            // 获取内容的两种方式
            // 1. 通过下标获取
            //System.out.print(csvReader.get(0));
            // 2. 通过表头的文字获取
            //System.out.println(" " + csvReader.get("年龄"));
            PostInfo postInfo =new PostInfo();
            postInfoList.add(postInfo);
        }
        return postInfoList;
    }

    public static void main(String[] args) {

    }

}
