package com.vis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.vis.domain.PieInformation;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PieInfoMapper extends BaseMapper<PieInformation> {
}
