package com.vis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.vis.domain.PositionInfo;
import org.apache.ibatis.annotations.Mapper;

import java.awt.image.BandedSampleModel;

@Mapper
public interface PositionInfoMapper extends BaseMapper<PositionInfo> {
}
