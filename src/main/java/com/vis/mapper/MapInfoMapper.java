package com.vis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.vis.domain.MapInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MapInfoMapper extends BaseMapper<MapInfo> {
}
