package com.vis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.vis.domain.DegreeSalary;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DegreeSalaryMapper extends BaseMapper<DegreeSalary> {
}
