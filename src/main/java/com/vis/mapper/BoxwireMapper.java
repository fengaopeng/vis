package com.vis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.vis.domain.Boxwire;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BoxwireMapper extends BaseMapper<Boxwire> {
}
