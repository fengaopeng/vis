package com.vis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.vis.domain.GeneralInformation;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GenerallnformationMapper extends BaseMapper<GeneralInformation> {

}
