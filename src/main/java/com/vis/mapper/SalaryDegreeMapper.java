package com.vis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.vis.domain.SalaryDegree;
import com.vis.vo.BarVo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SalaryDegreeMapper extends BaseMapper<SalaryDegree> {
}
