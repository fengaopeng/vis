package com.vis.domain;

import lombok.Data;

@Data
public class Funnel {
    private String name;

    private Integer value1;

    private Integer value2;

    private Integer value3;

    private Integer value4;

    private Integer value5;

    private Integer value6;

    private Integer value7;

    private Integer value8;

    private Integer value9;

    private Integer value10;

}
