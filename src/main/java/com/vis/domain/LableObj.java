package com.vis.domain;

import lombok.Data;

import java.util.List;

@Data
public class LableObj {
    private String value;

    private String label;

    private List<LableData> children;
}
