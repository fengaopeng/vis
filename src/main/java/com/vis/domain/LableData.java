package com.vis.domain;

import lombok.Data;

@Data
public class LableData {
    private String value;

    private String label;
}
