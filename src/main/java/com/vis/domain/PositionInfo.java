package com.vis.domain;

import lombok.Data;

@Data
public class PositionInfo {
    private String name;

    private Integer num;
}
