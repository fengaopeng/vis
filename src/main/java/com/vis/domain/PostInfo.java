package com.vis.domain;

import lombok.Data;

@Data
public class PostInfo {
    private String city;

    private String companyName;

    private String companyShortName;

    private String companySize;

    private String createTime;

    private String education;

    private String financeStage;

    private String industryField;

    private String jobNature;

    private String positionAdvantage;

    private String positionFirstType;

    private String positionName;

    private String positionType;

    private String Salary;

    private String workYear;

    private Integer companyId;

    private Integer positionId;

}
