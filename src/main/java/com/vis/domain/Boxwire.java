package com.vis.domain;

import lombok.Data;

@Data
public class Boxwire {
    private String x;

    private Integer low;

    private Integer q1;

    private Integer median;

    private Integer q3;

    private Integer high;
}
