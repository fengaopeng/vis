package com.vis.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "generalInformation")
public class GeneralInformation {

    private String name;

    private Integer count;
}
