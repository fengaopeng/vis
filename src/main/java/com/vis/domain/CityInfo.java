package com.vis.domain;

import lombok.Data;

@Data
public class CityInfo {
    private String cityName;

    private String count;

    private double lng;

    private double lat;
}
