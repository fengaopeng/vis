package com.vis.domain;

import lombok.Data;

@Data
public class SalaryDegree {
    private String name;

    private String type;

    private String salary;

    private Integer vocation;

    private Integer unlimited;

    private Integer undergraduate;

    private Integer master;

}
