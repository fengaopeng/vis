package com.vis.domain;

import lombok.Data;

@Data
public class DegreeSalary {
  private String name;

  private String type;

  private String degree;

  private Integer level1;

  private Integer level2;

  private Integer level3;

  private Integer level4;
}
