package com.vis.domain;

import lombok.Data;

@Data
public class MapInfo {
    private String time;

    private Integer num;
}
