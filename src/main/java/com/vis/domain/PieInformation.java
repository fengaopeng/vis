package com.vis.domain;

import lombok.Data;

@Data
public class PieInformation {
    private String name;

    private String type;

    private Integer vocation;

    private Integer unlimited;

    private Integer undergraduate;

    private Integer master;

    private Integer level1;

    private Integer level2;

    private Integer level3;

    private Integer level4;
}
