package com.vis.common;

import lombok.Data;

@Data
public class PieVo<T> {
    private Integer code;

    private String msg; //错误信息

    private T data; //数据

    private T pieData;

    private T roseData;

    public static <T> PieVo<T> success(String msg,T pieData,T roseData) {
        PieVo<T> r = new PieVo<T>();
        r.msg = msg;
        //r.data = null;
        r.pieData = pieData;
        r.roseData = roseData;
        r.code = 200;
        return r;
    }
}
