package com.vis.common;

import lombok.Data;

import java.util.Map;

@Data
public class ResultVo<T> {
    private Integer code;

    private String msg; //错误信息

    private T data; //数据

    private Map<String,String> geoCoordMap;

    public static <T> ResultVo<T> success(T object,Map geoCoordMap) {
        ResultVo<T> r = new ResultVo<>();
        r.msg = "success";
        r.data = object;
        r.code = 200;
        r.geoCoordMap = geoCoordMap;
        return r;
    }
}
