package com.vis.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class R<T> {

    private Integer code;

    private String msg; //错误信息

    private T data; //数据

    private T barData;

    private T LineData;

    private T pieData;

    private T roseData;

    public static <T> R<T> success(T barData,T LineData) {
        R<T> r = new R<T>();
        r.msg = "success";
        r.barData = barData;
        r.LineData = LineData;
        r.code = 200;
        return r;
    }

    public static <T> R<T> success(String msg,T pieData,T roseData) {
        R<T> r = new R<T>();
        r.msg = msg;
        r.pieData = pieData;
        r.roseData = roseData;
        r.code = 200;
        return r;
    }
    //private Map map = new HashMap(); //动态数据

    public static <T> R<T> success(T object) {
        R<T> r = new R<T>();
        r.msg = "success";
        r.data = object;
        r.code = 200;
        return r;
    }
    //操作成功后的数据封装
    public static <T> R<T> success(T data,String msg) {
        R<T> r = new R<T>();
        r.code = 200;
        r.msg = msg;
        r.data = data;
        return r;
    }
    public static <T> R<T> success(String msg) {
        R<T> r = new R<T>();
        r.msg = msg;
        r.code = 200;
        return r;
    }

    public static <T> R<T> error(String msg) {
        R r = new R();
        r.msg = msg;
        r.code = 200;
        return r;
    }



    public static <T> R<T> error(Integer code,String msg){
        R<T> r = new R<T>();
        r.code = code;
        r.data = null;
        r.msg = msg;
        return  r;
    }
}