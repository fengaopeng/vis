//package com.vis.filter;
//import com.alibaba.druid.util.StringUtils;
//
//import javax.servlet.*;
//import javax.servlet.annotation.WebFilter;
//import javax.servlet.annotation.WebInitParam;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.Arrays;
//import java.util.List;
//
////@WebFilter(filterName = "corsFilter", urlPatterns = "/*",
////        initParams = {@WebInitParam(name = "allowOrigin", value = "*"),
////                @WebInitParam(name = "allowMethods", value = "GET,POST,PUT,DELETE,OPTIONS"),
////                @WebInitParam(name = "allowCredentials", value = "true"),
////                @WebInitParam(name = "allowHeaders", value = "Content-Type,token")})
//public class CrosFilter implements Filter{
////    private String allowOrigin;
////    private String allowMethods;
////    private String allowCredentials;
////    private String allowHeaders;
////    private String exposeHeaders;
////
////    @Override
////    public void init(FilterConfig filterConfig) throws ServletException {
////        allowOrigin = filterConfig.getInitParameter("allowOrigin");
////        allowMethods = filterConfig.getInitParameter("allowMethods");
////        allowCredentials = filterConfig.getInitParameter("allowCredentials");
////        allowHeaders = filterConfig.getInitParameter("allowHeaders");
////        exposeHeaders = filterConfig.getInitParameter("exposeHeaders");
////    }
////
////    @Override
////    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
////        HttpServletRequest request = (HttpServletRequest) servletRequest;
////        HttpServletResponse response = (HttpServletResponse) servletResponse;
////        if (!StringUtils.isEmpty(allowOrigin)) {
////            if(allowOrigin.equals("*")){
////                // 设置哪个源可以访问
////                response.setHeader("Access-Control-Allow-Origin", allowOrigin);
////            }else{
////                List<String> allowOriginList = Arrays.asList(allowOrigin.split(","));
////                if (allowOriginList != null && allowOriginList.size() > 0) {
////                    String currentOrigin = request.getHeader("Origin");
////                    if (((List<?>) allowOriginList).contains(currentOrigin)) {
////                        response.setHeader("Access-Control-Allow-Origin", currentOrigin);
////                    }
////                }
////            }
////        }
////        if (!StringUtils.isEmpty(allowMethods)) {
////            //设置哪个方法可以访问
////            response.setHeader("Access-Control-Allow-Methods", allowMethods);
////        }
////        if (!StringUtils.isEmpty(allowCredentials)) {
////            // 允许携带cookie
////            response.setHeader("Access-Control-Allow-Credentials", allowCredentials);
////        }
////        if (!StringUtils.isEmpty(allowHeaders)) {
////            // 允许携带哪个头
////            response.setHeader("Access-Control-Allow-Headers", allowHeaders);
////        }
////        if (!StringUtils.isEmpty(exposeHeaders)) {
////            // 允许携带哪个头
////            response.setHeader("Access-Control-Expose-Headers", exposeHeaders);
////        }
////        filterChain.doFilter(servletRequest, servletResponse);
////    }
////
////    @Override
////    public void destroy() {
////
////    }
//    @Override
//    public void init(FilterConfig filterConfig) throws ServletException {
//    }
//    @Override
//    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
//        HttpServletRequest request = (HttpServletRequest) servletRequest;
//        HttpServletResponse response = (HttpServletResponse) servletResponse;
//        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
//        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
//        response.setHeader("Access-Control-Max-Age", "3600");
//        response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
//        response.setHeader("Access-Control-Allow-Credentials", "true");
//        System.out.println("经过过滤器放行！");
//        filterChain.doFilter(servletRequest, servletResponse);
//    }
//    @Override
//    public void destroy() {
//    }
//}
