package com.vis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.vis.domain.PieInformation;

public interface PieInfoService extends IService<PieInformation> {
}
