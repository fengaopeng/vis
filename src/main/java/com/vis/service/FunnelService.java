package com.vis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.vis.domain.Funnel;

public interface FunnelService extends IService<Funnel> {
}
