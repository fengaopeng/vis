package com.vis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vis.domain.SalaryDegree;
import com.vis.mapper.SalaryDegreeMapper;
import com.vis.service.SalaryDegreeService;
import org.springframework.stereotype.Service;

@Service
public class SalaryDegreeServiceImpl extends ServiceImpl<SalaryDegreeMapper, SalaryDegree> implements SalaryDegreeService {

}
