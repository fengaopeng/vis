package com.vis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vis.domain.DegreeSalary;
import com.vis.mapper.DegreeSalaryMapper;
import com.vis.service.DegreeSalaryService;
import org.springframework.stereotype.Service;

@Service
public class DegreeSalaryServiceImpl extends ServiceImpl<DegreeSalaryMapper, DegreeSalary> implements DegreeSalaryService {
}
