package com.vis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vis.domain.PositionInfo;
import com.vis.mapper.PositionInfoMapper;
import com.vis.service.PositionInfoService;
import org.springframework.stereotype.Service;

@Service
public class PositionInfoServiceImpl extends ServiceImpl<PositionInfoMapper, PositionInfo> implements PositionInfoService {
}
