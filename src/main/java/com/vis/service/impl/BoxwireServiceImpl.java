package com.vis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vis.domain.Boxwire;
import com.vis.mapper.BoxwireMapper;
import com.vis.service.BoxwireService;
import org.springframework.stereotype.Service;

@Service
public class BoxwireServiceImpl extends ServiceImpl<BoxwireMapper, Boxwire> implements BoxwireService {
}
