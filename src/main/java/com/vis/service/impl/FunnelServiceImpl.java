package com.vis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vis.domain.Funnel;
import com.vis.mapper.FunnelMapper;
import com.vis.service.FunnelService;
import org.springframework.stereotype.Service;

@Service
public class FunnelServiceImpl extends ServiceImpl<FunnelMapper, Funnel> implements FunnelService {
}
