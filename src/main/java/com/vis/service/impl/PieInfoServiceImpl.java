package com.vis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vis.domain.PieInformation;
import com.vis.mapper.PieInfoMapper;
import com.vis.service.PieInfoService;
import org.springframework.stereotype.Service;

@Service
public class PieInfoServiceImpl extends ServiceImpl<PieInfoMapper, PieInformation> implements PieInfoService {
}
