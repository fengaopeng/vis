package com.vis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.vis.domain.MapInfo;
import com.vis.mapper.MapInfoMapper;
import com.vis.service.MapInfoService;
import org.springframework.stereotype.Service;

@Service
public class MapInfoServiceImpl extends ServiceImpl<MapInfoMapper, MapInfo> implements MapInfoService {
}
