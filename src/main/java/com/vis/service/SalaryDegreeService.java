package com.vis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.vis.domain.SalaryDegree;

public interface SalaryDegreeService extends IService<SalaryDegree> {
}
