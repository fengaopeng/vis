package com.vis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.vis.domain.MapInfo;

public interface MapInfoService extends IService<MapInfo> {
}
