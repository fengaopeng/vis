package com.vis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.vis.domain.PositionInfo;

public interface PositionInfoService extends IService<PositionInfo> {
}
