package com.vis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.vis.domain.CityInfo;

public interface CityInfoService extends IService<CityInfo> {
}
