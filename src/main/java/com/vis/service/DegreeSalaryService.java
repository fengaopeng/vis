package com.vis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.vis.domain.DegreeSalary;

public interface DegreeSalaryService extends IService<DegreeSalary> {
}
